public class SimpleWar {
	public static void main(String[] args) {
		//Step 1 and 2
		Deck Cards = new Deck();
		Cards.shuffle();
		//Step 3
		int PlayerOnePoints = 0;
		int PlayerTwoPoints = 0;
		//Step 4
		System.out.println(Cards.drawTopCard());
		System.out.println(Cards.drawTopCard());
		//Step 6
		Card topcard = Cards.drawTopCard();
		System.out.println("you drew the " + topcard);
		System.out.println(topcard.calculateScore());
		//Step 8
		while (Cards.length() > 1) {
			//Step 7
			Card cardone = Cards.drawTopCard();
			Card cardtwo = Cards.drawTopCard();
			System.out.println("Player One Drew " + cardone + " It ammounts to " + cardone.calculateScore());
			System.out.println("Player Two Drew " + cardtwo + " It ammounts to " + cardtwo.calculateScore());
		
			if (cardone.calculateScore() > cardtwo.calculateScore()) {
				System.out.println("Player One Gets A Point");
				PlayerOnePoints++;
			} else {
				System.out.println("Player Two Gets A Point");
				PlayerTwoPoints++;
			}
			System.out.println(PlayerOnePoints);
			System.out.println(PlayerTwoPoints);
		}
		if (PlayerOnePoints > PlayerTwoPoints) {
			System.out.println("Player One Wins!");
		} else {
			System.out.println("Player Two Wins!");
		}
	}
}