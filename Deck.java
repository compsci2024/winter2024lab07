import java.util.Random;
 
public class Deck {
	//Set the Attributes
    private Card[] myDeck;
    public int numOfCards;
    private Random rng;
	private Card centerCard;
    private Card playerCard;
	//Add the Constructor
    public Deck() {
		this.rng = new Random();
        this.numOfCards = 52;
        myDeck = new Card[numOfCards];
        String[] suits = {"clubs", "clovers", "hearts", "diamonds"};
        String[] ranks = {"Ace", "2", "3", "4", "5", "6", "7", "8", "9", "10", "Jack", "Queen", "King"};
		//iterate to make each card in the deck
        int i = 0;
        for (String rank : ranks) {
            for (String suit : suits) {
                myDeck[i++] = new Card(suit, rank);
            }
        }
    }
	public Card[] getMyDeck() {
        return myDeck;
    }
	public int length() {
		return this.numOfCards;
	}
	public Card drawTopCard() {
		Card TopCard = myDeck[numOfCards-1];
		myDeck[numOfCards-1] = null;
		this.numOfCards--;
		return TopCard;

	}
	

	
	public void shuffle() {
        for (int i = numOfCards - 1; i > 0; i--) {
            int randomcard = rng.nextInt(i + 1);
            //Swap
            Card temp = myDeck[randomcard];
            myDeck[randomcard] = myDeck[i];
            myDeck[i] = temp;
        }
    }
	public Card getcenterCard() {
        return myDeck[numOfCards - 1];
    }

    public Card getplayerCard() {
        return myDeck[numOfCards - 2];
    }
}