public class Card {
	//Declare Attributes
	private String rank;
	private String suit;
	//Add the Constructor for the Suit and Rank
	public Card(String suit, String rank) {
		this.rank = rank;
		this.suit = suit;
	}
	//Getters for Both Suit and Rank, no need for Setters
	public String GetSuit() {
		return this.suit;
	}
	public String GetRank() {
		return this.rank;
	}
	//To String method for printing the Card
	public String toString() {
		return (rank + " of " + suit);
	}
	//Starting Lab 7A
	//Step 5
	public double calculateScore() {
		double suitconvert = 0;
		if (suit.equals("hearts")) {
			suitconvert = 0.4;
		} else if (suit.equals("spades")) {
			suitconvert = 0.3;
		} else if (suit.equals("diamonds")) {
			suitconvert = 0.2;
		} else {
			suitconvert = 0.1;
		}
		double rankconvert = 0;
		if (rank.equals("Ace")) {
			rankconvert = 1;
		} else if (rank.equals("Jack")) {
			rankconvert = 11;
		} else if (rank.equals("Queen")) {
			rankconvert = 12;
		} else if (rank.equals("King")) {
			rankconvert = 13;
		} else {
			rankconvert = Double.parseDouble(rank);
		}
	
		return rankconvert+suitconvert;
	}
}